jQuery(document).ready(function(){
  jQuery(".gallery-item a").colorbox(
    {
      rel:"sketches",
      fixed:"true",
      maxHeight:"90%",
      maxWidth:"90%",
      opacity: 0.8,
      closeButton: false,
      previous: "<",
      next: ">",
      current: "",
      transition: "none"
    });
});
