var path = require('path');

module.exports = {
	entry: "./entry.js",
	output: {
		path: __dirname,
		filename: "bundle.js"
	},
	module: {
		loaders: [
			{ test: /\.css$/, loader: "style!css" },
			{ test: /\.scss$/, loader: "style!css!sass?includePaths[]=" +
        path.resolve(__dirname, "./something-fresh/sass/") }
		]
	},
	externals: {
	  // require("jquery") is external and available
	  //  on the global var jQuery
	  "jquery": "jQuery"
  }
};
