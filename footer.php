<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Something Fresh
 */
?>

	</div><!-- #content -->

	<footer id="site-footer" class="site-footer" role="navigation">
			<?php wp_nav_menu( array( 'theme_location' => 'secondary', 'container' => 'nav', 'container_class' => 'footer-navigation' ) ); ?>
			<?php if ( function_exists('cn_social_icon') ) echo cn_social_icon(); ?>
	</footer><!-- #site-footer -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
